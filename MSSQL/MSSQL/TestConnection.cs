﻿using System;
using System.Text;
using System.Data.SqlClient;

namespace MSSQL
{
    class TestConnection
    {
        static void oMain(string[] args)
        {
            try
            {
                // Build connection string
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "192.168.88.127";   // update me
                builder.UserID = "u2";              // update me
                builder.Password = "passwd";      // update me
                builder.InitialCatalog = "CtvrtekDB";

                // Connect to SQL
                Console.Write("Connecting to SQL Server ... ");
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    Console.WriteLine("Done.");
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("\n\nERROR EX.:" + e.ToString());
            }

            Console.WriteLine("All done. Press any key to finish...");
            Console.ReadKey(true);
        }
    }
}
﻿using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamostatnyProjektOracle
{
    class DatabaseConnector
    {
        string connstr;

        public DatabaseConnector(string jmeno, string heslo)
        {
            connstr = "Data Source=(DESCRIPTION =(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)" +
                "(HOST = m5w10.dom890.vsb.cz)(PORT = 1521)))" +
                "(CONNECT_DATA =(SERVICE_NAME = xe)));User ID=" + jmeno + ";" +
                "Password=" + heslo + ";";
        }

        public bool AddNewUser(string firstname, string surname)
        {
            string sqlcommand = "INSERT INTO cviko.\"users\" (id,firstname,lastname) " +
                "values (cviko.sequserpk.nextval,'" + firstname + "','" + surname + "')";
            bool output = false;

            using (var oracon = new OracleConnection())
            {
                oracon.ConnectionString = connstr;
                using (var cmd = new OracleCommand())
                {
                    cmd.Connection = oracon;
                    cmd.CommandText = sqlcommand;
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        oracon.Open();
                        if (cmd.ExecuteNonQuery() == 1)
                        {
                            output = true;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        output = false;
                    }
                    finally
                    {
                        oracon.Close();
                    }
                }
            }
            return output;
        }

        public List<User> GetUsers()
        {
            var output = new List<User>();
            string sqlcommand = "SELECT id, firstname, lastname FROM cviko.\"users\"";

            using (var oracon = new OracleConnection())
            {
                oracon.ConnectionString = connstr;
                using (var cmd = new OracleCommand())
                {
                    cmd.Connection = oracon;
                    cmd.CommandText = sqlcommand;
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        oracon.Open();
                        using (var rd = cmd.ExecuteReader())
                        {
                            User temporary;
                            while (rd.Read())
                            {
                                temporary = new User();
                                temporary.ID = (Int16)rd["id"];
                                temporary.Surname = (string)rd["lastname"];
                                temporary.Name = (string)rd["firstname"];
                                output.Add(temporary);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    finally
                    {
                        oracon.Close();
                    }
                }
                return output;
            }
        }
    }
}

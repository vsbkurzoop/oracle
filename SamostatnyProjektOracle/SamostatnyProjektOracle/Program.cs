﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamostatnyProjektOracle
{
    class Program
    {
        static void Main(string[] args)
        {
            var spojeni = new DatabaseConnector("u12", "passwd");
            Console.WriteLine("Zadej jméno:");
            string name = Console.ReadLine();
            Console.WriteLine("Zadej příjmení:");
            string surname = Console.ReadLine();
            if (spojeni.AddNewUser(name, surname))
            {
                Console.WriteLine("Řádek vložen");
            }
            else
            {
                Console.WriteLine("Řádek nevložen");
            }

            var kolekce = spojeni.GetUsers();
            foreach (var item in kolekce)
            {
                Console.WriteLine(item.ID + " " + item.Name + " " + item.Surname);
            }

            Console.WriteLine("Konec");
            Console.ReadKey();
        }
    }
}

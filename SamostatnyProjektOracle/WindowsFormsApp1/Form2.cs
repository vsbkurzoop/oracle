﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        List<User> users;
        int index = 0;

        public Form2()
        {
            InitializeComponent();

            users = new DatabaseConnectorMSSQL().GetUsers();
            fill();
        }

        private void fill()
        {
            textBox1.Text = users[index].Name;
            textBox2.Text = users[index].Surname;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (index < users.Count-1)
            {
                index++;
                fill();
            }
        }
    }
}

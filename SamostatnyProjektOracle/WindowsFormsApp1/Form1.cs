﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            var temp = new DatabaseConnectorMSSQL();
            if (firstname.Text != "" && surname.Text != "")
            {
                if (temp.AddNewUser(firstname.Text, surname.Text))
                {
                    firstname.Text = "";
                    surname.Text = "";
                    output_label.Text = "Do databáze byl úspešně přidán jeden řádek";
                }
                else
                {
                    output_label.Text = "Do databáze nebyl úspešně přidán jeden řádek";
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = new DatabaseConnector("u12", "passwd").GetUsersToDT();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            dataGridView1.DataSource = new DatabaseConnectorMSSQL().GetUsersToDT();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var temp = new DatabaseConnector("u12","passwd");
            if (firstname.Text != "" && surname.Text != "")
            {
                if (temp.AddNewUser(firstname.Text, surname.Text))
                {
                    firstname.Text = "";
                    surname.Text = "";
                    output_label.Text = "Do databáze byl úspešně přidán jeden řádek";
                }
                else
                {
                    output_label.Text = "Do databáze nebyl úspešně přidán jeden řádek";
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            new Form2().ShowDialog();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    class DatabaseConnectorMSSQL
    {
        string connstr;

        public DatabaseConnectorMSSQL()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "localhost\\SQLEXPRESS";
            builder.IntegratedSecurity = true;
            builder.InitialCatalog = "CtvrtekDB";
            connstr = builder.ConnectionString;
        }

        public bool AddNewUser(string firstname, string surname)
        {
            string sqlcommand = "INSERT INTO users (firstname,lastname) " +
                "VALUES ('" + firstname + "','" + surname + "')";
            bool output = false;

            using (var mscon = new SqlConnection())
            {
                mscon.ConnectionString = connstr;
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = mscon;
                    cmd.CommandText = sqlcommand;
                    cmd.CommandType = System.Data.CommandType.Text;
                    try
                    {
                        mscon.Open();
                        if (cmd.ExecuteNonQuery() == 1)
                        {
                            output = true;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        output = false;
                    }
                    finally
                    {
                        mscon.Close();
                    }
                }
            }
            return output;
        }

        public List<User> GetUsers()
        {
            var output = new List<User>();
            string sqlcommand = "SELECT id, firstname, lastname FROM users";

            using (var mscon = new SqlConnection())
            {
                mscon.ConnectionString = connstr;
                using (var cmd = new SqlCommand(sqlcommand,mscon))
                {
                    //cmd.Connection = mscon;
                    //cmd.CommandText = sqlcommand;
                    //cmd.CommandType = System.Data.CommandType.Text;
                    try
                    {
                        mscon.Open();
                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {
                            User temporary;
                            while (rd.Read())
                            {
                                temporary = new User();
                                temporary.ID = rd.GetInt32(0);
                                temporary.Surname = rd.GetString(1);
                                temporary.Name = rd.GetString(2);
                                output.Add(temporary);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    finally
                    {
                        mscon.Close();
                    }
                }
                return output;
            }
        }

        public DataTable GetUsersToDT()
        {
            string sqlcommand = "SELECT id, firstname, lastname FROM users";

            DataTable dt = new DataTable();
            using (var mscon = new SqlConnection())
            {
                mscon.ConnectionString = connstr;
                using (var cmd = new SqlCommand(sqlcommand, mscon))
                {
                    try
                    {
                        mscon.Open();
                        using (var rd = cmd.ExecuteReader())
                        {
                            dt.Load(rd);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    finally
                    {
                        mscon.Close();
                    }
                }
            }

            return dt;
        }
    }
}

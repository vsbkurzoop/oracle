﻿using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oracle
{
    public class OracleConnect
    {
        string connstr;

        public OracleConnect(string jmeno, string heslo)
        {
            connstr = "Data Source=(DESCRIPTION =(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)" +
                "(HOST = m5w10.dom890.vsb.cz)(PORT = 1521)))" +
                "(CONNECT_DATA =(SERVICE_NAME = xe)));User ID=" + jmeno + ";Password=" + heslo + ";";
            //connstr = "Data Source=orcl1;User ID=c##u2;Password=passwd;";
        }


        public int ExecuteNonQuery(string command)
        {
            int output;
            using (var oracon = new OracleConnection())
            {
                oracon.ConnectionString = connstr;
                using (var cmd = new OracleCommand())
                {
                    cmd.Connection = oracon;
                    cmd.CommandText = command;
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        oracon.Open();
                        output = cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        output = -1;
                    }
                    finally
                    {
                        oracon.Close();
                    }
                }
            }
            return output;
        }

        public void ExecuteQuery(string query)
        {
            using (var oracon = new OracleConnection())
            {
                oracon.ConnectionString = connstr;
                using (var cmd = new OracleCommand())
                {
                    cmd.Connection = oracon;
                    cmd.CommandText = query;
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        oracon.Open();
                        using (var rd = cmd.ExecuteReader())
                        {
                            while (rd.Read())
                            {

                                //for (int i = 0; i < rd.FieldCount; i++)
                                //{
                                //    if (!rd.IsDBNull(i))
                                //    {
                                //        Console.Write(rd.GetValue(i)+" ");
                                //    }
                                //}
                                Console.WriteLine(rd["id"]+" "+ rd["firstname"]+" "+ rd["lastname"]);

                                //Console.WriteLine();
                                //rd["NAZEVSLOUPCE"]
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    finally
                    {
                        oracon.Close();
                    }
                }
            }
        }
        public DataTable ExecuteQueryToDT(string query)
        {
            DataTable dt = new DataTable();
            using (var oracon = new OracleConnection())
            {
                oracon.ConnectionString = connstr;
                using (var cmd = new OracleCommand(query, oracon))
                {
                    try
                    {
                        oracon.Open();
                        using (var rd = cmd.ExecuteReader())
                        {
                            dt.Load(rd);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    finally
                    {
                        oracon.Close();
                    }
                }
            }

            return dt;
        }

        public bool TestConnection()
        {
            using (var oracon = new OracleConnection())
            {
                oracon.ConnectionString = connstr;
                try
                {
                    oracon.Open();
                    oracon.Close();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

    }
}

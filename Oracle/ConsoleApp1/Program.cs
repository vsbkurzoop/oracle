﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var oracmd = new Oracle.OracleConnect("u12", "passwd");
            Console.WriteLine(oracmd.TestConnection());
            //Console.WriteLine(oracmd.ExecuteNonQuery("insert into cviko.\"users\" " +
            //    "(id,lastname, firstname) values (cviko.sequserpk.nextval, 'Černý', 'Antonín')"));
            oracmd.ExecuteQuery("select * from cviko.\"users\"");
            //Console.WriteLine(oracmd.ExecuteNonQuery("update cviko.\"users\" set lastname = 'Černoch' where lastname = 'Černý'"));
            //oracmd.ExecuteQuery("select count(id) from c##u1.test_table");
            //var temp = oracmd.ExecuteQueryToDT("select * from c##u1.test_table");
            //foreach (DataRow item in temp.Rows)
            //{
            //    Console.WriteLine(item.Field<Int16>(0));
            //}

            Console.WriteLine("The end!");
            Console.ReadKey();
        }
    }
}
